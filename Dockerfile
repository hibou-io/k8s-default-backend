FROM golang:1.18 as golayer
RUN apt-get update -y && apt-get install -y ca-certificates

WORKDIR /app
COPY *.go /app
COPY go.mod /app

RUN go build -o k8s-default-backend 

FROM debian:stable-slim

COPY --from=golayer /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=golayer /app/k8s-default-backend /k8s-default-backend

ENTRYPOINT ["/k8s-default-backend"]


